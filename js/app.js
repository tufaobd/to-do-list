angular.module('ToDoListApp', []);
function ToDoList(){
  
  var vm = this;
  var model = {
    tarefas : [],
    formNovoPostIt : {
      titulo : null,
      texto : null,
    }
  };
  function addPostIt(){
    
    var tarefa = angular.copy(vm.model.formNovoPostIt);
    vm.model.tarefas.push(tarefa);
  }
  function removerPostIt(tarefa){
    var pos = vm.model.tarefas.map(function(e){
      return e.titulo;
    })
    .indexOf(tarefa.titulo);
    vm.model.tarefas.splice(pos,1);
  }
  angular.extend(this, {
    model : model,
    removerPostIt : removerPostIt,
    addPostIt : addPostIt
  });
}
angular
  .module('ToDoListApp')
  .controller('ToDoList', ToDoList);